import Knight
import Dragon
import requests
from xml.etree import ElementTree


class Battle:
    def __init__(self):
        self.gameId = 0
        self.knight = 0
        self.dragon = 0
        self.weather = ""

    # Gets battle info from API
    def new_battle(self, url):
        json = requests.get(url).json()
        self.gameId = json["gameId"]
        self.knight = Knight.Knight(json["knight"])
        self.dragon = Dragon.Dragon()
        return json

    # Gets weather info from API
    def set_weather(self, url):
        text = requests.get(url.format(self.gameId)).text
        tree = ElementTree.fromstring(text)
        self.weather = tree.findall("message")[0].text
        return self.weather

    # Sends a response to API
    def solve_battle(self, url):
        self.dragon.get_dragon(self.knight, self.weather)
        data = {}
        # In case of a storm, don't send a dragon
        if "storm" not in self.weather:
            data = self.dragon.get_json()
        response = requests.put(url.format(self.gameId), data=data, headers={"Content-Type": "application/json"})
        return response


if __name__ == "__main__":
    print("Run main.py")
