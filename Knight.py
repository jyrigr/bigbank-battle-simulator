# Holds knights stats
class Knight:
    def __init__(self, json):
        self.name = json["name"]
        self.stats = json
        del self.stats['name']

    def __str__(self):
        return str(self.stats)
