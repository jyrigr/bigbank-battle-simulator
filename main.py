import sys
import datetime
import Battle

# CONFIGURATION #
startUrl = "http://www.dragonsofmugloar.com/api/game"
weatherUrl = "http://www.dragonsofmugloar.com/weather/api/report/{}"
solveUrl = "http://www.dragonsofmugloar.com/api/game/{}/solution"

help = """
    How to use battle solver:
    
    -C  count       Number of battles
    -L  filename    Log file name
        """

totalBattles = 0
wonBattles = 0

if __name__ == "__main__":
    # If no arguments given, print out help
    if len(sys.argv) < 2:
        print(help)

    # Default arguments
    count = 10
    logname = "log.log"
    # Checking for arguments
    try:
        count = int(sys.argv[sys.argv.index("-C")+1])
    except:
        print("Running with default count (10)")

    try:
        logname = sys.argv[sys.argv.index("-L") + 1]
    except:
        print("Running with default log file name (log.log)\n")

    file = open(logname, "w")
    file.write(datetime.datetime.now().strftime('%d-@m-%Y %H:%M:%S') + "\n")
    for i in range(count):
        # Battle logic
        totalBattles += 1
        battle = Battle.Battle()
        battle.new_battle(startUrl)
        battle.set_weather(weatherUrl)
        result = battle.solve_battle(solveUrl)
        # Writing to log file
        file.write("GameID: " + str(battle.gameId) + "\n")
        file.write(str(battle.knight) + "\n")
        file.write(str(battle.dragon) + "\n")
        file.write(battle.weather + "\n")
        file.write(str(result.json()) + "\n\n")

        if result.json()['status'] == "Victory":
            wonBattles += 1

        # Live counter for battles on CLI
        print("\rBattles played: {}".format(totalBattles) + ". " +
              "Battles won: {}".format(wonBattles) + ". " +
              "Win rate: {:.2f}%".format((wonBattles / totalBattles) * 100)
              , end="")
    file.close()

