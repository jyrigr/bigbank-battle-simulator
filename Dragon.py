import json
import operator

# knight stat name to opposing dragon stat dict
stattostat = {
    "attack": "scaleThickness",
    "armor": "clawSharpness",
    "agility": "wingStrength",
    "endurance": "fireBreath"}


class Dragon:
    def __init__(self):
        # Base dragon is zen for fog and long dry weather
        self.stats = \
            {"scaleThickness": 5,
             "clawSharpness": 5,
             "wingStrength": 5,
             "fireBreath": 5}

    # Returns json of the dragon to PUT
    def get_json(self):
        temp = {"dragon": self.stats}
        return json.dumps(temp)

    # Finds the best dragon for a knight and weather
    def get_dragon(self, knight, weather):
        if "regular" in weather:
            # Sorts stats to key: value pairs that are sorted by values
            sortedstats = sorted(knight.stats.items(), key=operator.itemgetter(1))
            self.stats[stattostat[sortedstats[3][0]]] = sortedstats[3][1] + 2
            self.stats[stattostat[sortedstats[2][0]]] = sortedstats[2][1]
            self.stats[stattostat[sortedstats[1][0]]] = sortedstats[1][1] - 1
            self.stats[stattostat[sortedstats[0][0]]] = sortedstats[0][1] - 1
            self.balance()
        elif "flood" in weather:
            sortedstats = sorted(knight.stats.items(), key=operator.itemgetter(1))
            self.stats[stattostat[sortedstats[3][0]]] = sortedstats[3][1] + 2
            self.stats[stattostat[sortedstats[2][0]]] = sortedstats[2][1]
            self.stats[stattostat[sortedstats[1][0]]] = sortedstats[1][1] - 1
            self.stats[stattostat[sortedstats[0][0]]] = sortedstats[0][1] - 1
            self.stats["clawSharpness"] += self.stats["fireBreath"]
            self.stats["fireBreath"] = 0
            self.balance()
        return self

    # Checks for extreme values and corrects them
    def balance(self):
        # Sorts stats to key: value pairs that are sorted by values
        sortedstats = sorted(self.stats.items(), key=operator.itemgetter(1))

        if sum(self.stats.values()) > 20:
            self.stats[sortedstats[3][0]] -= sum(self.stats.values()) - 20
            self.balance()

        if sum(self.stats.values()) < 20:
            self.stats[sortedstats[3][0]] += 20 - sum(self.stats.values())
            self.balance()

        if max(self.stats.values()) > 10:
            self.stats[sortedstats[3][0]] = 10
            self.stats[sortedstats[2][0]] += sortedstats[3][1] - 10

        if min(self.stats.values()) < 0:
            self.stats[sortedstats[0][0]] = 0
            self.stats[sortedstats[2][0]] += sortedstats[0][1]

        return self

    def __str__(self):
        return str(self.get_json())

